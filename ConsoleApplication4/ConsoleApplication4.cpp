﻿#include <iostream> 
#include <cmath> 

class Vector 
{
private:
	double x = 0;
	double y = 0;
	double z = 0;

public:
	Vector(double x, double y, double z) : x(x), y(y), z(z) 
	{}

	void Show() 
	{
		std::cout << "Vector(" << x << ", " << y << ", " << z << ")" << std::endl;
	}

	double Length() 
	{
		return std::sqrt(x*x + y*y + z*z);
	}
};

int main() 
{

	Vector vector(1, 2, 3);
	vector.Show();
	std::cout << "Length is " << vector.Length() << std::endl;

}
